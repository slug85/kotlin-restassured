
import io.restassured.builder.RequestSpecBuilder
import io.restassured.filter.log.LogDetail
import io.restassured.module.kotlin.extensions.Given
import io.restassured.module.kotlin.extensions.Then
import io.restassured.module.kotlin.extensions.When
import io.restassured.specification.RequestSpecification
import org.testng.annotations.BeforeTest
import org.testng.annotations.Test
import ru.yandex.qatools.allure.annotations.Features
import ru.yandex.qatools.allure.annotations.Stories

class WMSTest{

    private lateinit var specs: RequestSpecification

    @BeforeTest
    fun setUp(){
        specs = RequestSpecBuilder()
            .setBaseUri(System.getProperty("wms.api"))
            .addHeaders(mutableMapOf<String, String>())
            .log(LogDetail.ALL)
            .build()
    }

    @Test
    @Features("WMS")
    @Stories("bill")
    fun createBill(){

        val json = read("data/wms/bill.json")

        Given {
            spec(specs)
            body(json)
            queryParam("api_token", "95b7b28f4a00965ff7c7")
        } When {
            post("cash/bill")
        } Then {
            statusCode(200)
        }
    }

}

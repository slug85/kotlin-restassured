
import com.google.common.io.Resources.getResource

fun read(fileName: String): String {
    return getResource(fileName).readText()
}